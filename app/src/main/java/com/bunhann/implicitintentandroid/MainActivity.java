package com.bunhann.implicitintentandroid;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnImp1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnImp1 = findViewById(R.id.buttonBrowser);
        btnImp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.google.com"));
                startActivity(intent);
            }
        });
    }

    protected void intentEmail(View view){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto: abc@xyz.com"));
        startActivity(Intent.createChooser(emailIntent, "Send feedback"));
    }

    protected void intentFB(View view) {
        ShareUtils.shareFacebook(this, "Share on Android", "https://developers.facebook.com/");
    }

}
